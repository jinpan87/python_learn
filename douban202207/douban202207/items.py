# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field, Item


class Douban202207Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class Result(object):
    def __int__(self, code, list, msg, success):
        self.code = code
        self.data = list
        self.msg = msg
        self.success = success


class Data(object):
    def __int__(self, ip, port, ):
        self.ip = ip
        self.port = port
