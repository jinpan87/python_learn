#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: jingdong_spider.py
@date: 2022/6/27 18:36
@desc:
"""


class JingdongSpider(scrapy.Spider):
    name = "jingdong"

    def start_requests(self):
        urls = [
            'https://movie.douban.com/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response, **kwargs):
        self.log(response)
