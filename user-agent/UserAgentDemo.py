#!/usr/bin/env python
# -*- coding: utf-8 -*

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: UserAgentDemo.py
@date: 2022/5/27 10:28
@desc: 自动生成 user-agent
"""


def print_hi():
    n = 1
    while n < 10:
        yield n
        n += 1


if __name__ == '__main__':
    num = print_hi()
    print(num.__next__())
    # print('迭代器值：'+num.__next__())
    # for n in num:
    #     print(n)
