# -*- coding: utf-8 -*-
# @Time    : 2021/10/26 9:30
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test01.py
# @Software: PyCharm

def print_hi(name):
    print(f'Hi, {name}')


if __name__ == '__main__':
    print_hi('Python')
