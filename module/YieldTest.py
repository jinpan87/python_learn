#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: YieldTest.py
@date: 2022/7/9 11:23
@desc:
"""


def print_hi(name):
    print(f'Hi, {name}')


def foo():
    print("start fun ...")
    while True:
        res = yield 4
        print('res=', res)


if __name__ == '__main__':
    # print_hi('Python')
    go = foo()
    print(next(go))
    print('*'*20)
    print(next(go))
    # print(next(go))
    # print(next(go))
    # print(next(go))
    # print(next(go))

