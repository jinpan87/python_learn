# -*- coding: utf-8 -*-
# @Time    : 2021/10/26 9:13
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : test.py
# @Software: PyCharm


import sys


def test():
    args = sys.argv
    if len(args) == 1:
        print("hello world ")
    elif len(args) == 2:
        print("hello, %s!" % args[1])
    else:
        print("too many arguments")


def arr():
    topic = (1, 2, 3, 4)


'''
字符串格式化
'''


def str_test():
    name = 'marry'
    print(f'hello {name} ')
    print("hello {0}".format('jack'))
    print("hello,%s" % "lucy")
    print("i have {0:.1f}".format(12.33))


def list_tuple_test():
    list_a = [1, 'a', [3, 4, 'b'], 'd']
    tuple_b = (2, 4, 6, list_a)
    print(list_a[2])
    print(list_a[-1])
    list_a.pop()
    print(list_a)
    list_a.pop(1)
    print(list_a)
    list_a.insert(2, 'esc')
    print(list_a)

    print(tuple_b[3])
    print(tuple_b[3][2])


def for_test():
    y = 1

    n = 0

    while y:
        n = n + 1

        print(n)


if __name__ == '__main__':
    # topic = (1, 2, 3, 4)
    # for a in topic:
    #     print(a)
    # str_test()
    # list_tuple_test()
    for_test()