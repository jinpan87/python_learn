# -*- coding: utf-8 -*-
'''
@Time    : 2022/12/14 9:51
@Author  : robin
@File    : ListGenerate.py
@Software: PyCharm
'''


def print_hi(name):
    print(f'Hi,{name}')
    l = [x for x in range(1, 11) if x % 2 == 0]
    print(l)
    print(isinstance(l, list))

    ll = [x for x in range(100)]
    var = ll[2::3]
    print(var)


def trim(str):
    if str[:1] == " ":
        return trim(str[1:])
    elif str[-1:] == " ":
        return trim(str[:-1])
    else:
        return str


def findMinAndMax(L):
    if (len(L) == 0):
        return (None, None)
    else:
        min = max = L[0]
        for i in L:
            if max < i:
                max = i
            if min > i:
                min = i
        return (max, min)


if __name__ == '__main__':
    # if trim("hello ") != "hello":
    #     print("错误")
    # elif trim(" hello") != "hello":
    #     print("错误2")
    # elif trim(" hello ") != "hello":
    #     print("错误3")
    # elif trim(" hello world ") != "hello world":
    #     print("错误4")
    # else:
    #     print("成功")
    print(findMinAndMax([]))
    print(findMinAndMax([1]))
    print(findMinAndMax([1, 3, 9, 8, 16]))
