# -*- coding: utf-8 -*-
# @Time    : 2021/11/4 9:32
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : use_property.py
# @Software: PyCharm
class Student(object):

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be integer')
        if 0 > value or value > 100:
            raise ValueError('score must bettwen 0 ~ 100')
        self._score = value


s = Student()
s.score = 60
print(s.score)
s.score = 999
print(s.score)
