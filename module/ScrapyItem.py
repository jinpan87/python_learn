#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: ScrapyItem.py
@date: 2022/1/15 10:56
@desc:
"""

import scrapy


class Product(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    stock = scrapy.Field()
    tags = scrapy.Field()
    last_updated = scrapy.Field(serializer=str)


def print_hi(name):
    print(f'Hi, {name}')


if __name__ == '__main__':
    print_hi('Python')
    product = Product(name='robin', price=1000)
    print(product)
    print(product.get('name'))
    print(product['price'])
    # print(product['last_updated'])
    print(product.get('last_updated', 'not set'))
