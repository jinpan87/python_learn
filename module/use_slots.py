# -*- coding: utf-8 -*-
# @Time    : 2021/11/4 9:15
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : use_slots.py
# @Software: PyCharm

class Student(object):
    __slots__ = ('name', 'age')


class GraduteStudent(Student):
    pass


s = Student()
s.name = 'robin'
s.age = 20
print('%s,%s', s.name, s.age)

try:
    s.score = 90

except AttributeError as e:
    print("attributeError", e)

g = GraduteStudent()
g.score = 99
print(g.score)
