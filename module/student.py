# -*- coding: utf-8 -*-
# @Time    : 2021/11/3 9:20
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : student.py
# @Software: PyCharm

class Student(object):
    def __init__(self, name, score):
        self.__name = name
        self.__score = score

    def print_score(self):
        print('%s:%s' % (self.__name, self.__score))

    def get_name(self):
        return self.__name

    def get_score(self):
        return self.__score

    def set_score(self, score):
        if 0 < score < 100:
            self.__score = score
        else:
            raise ValueError('bad score')

    def grade_core(self):
        if self.score > 90:
            print('a')
        elif self.score > 60:
            print('b')
        else:
            print('c')


robin1 = Student('robin1', 59)
robin2 = Student('robin2', 90)

# robin1.print_score()
# robin2.grade_core()

robin1.set_score(80)
print(robin1._Student__name)
print(robin1.get_score())
