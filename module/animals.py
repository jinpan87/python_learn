# -*- coding: utf-8 -*-
# @Time    : 2021/11/3 9:54
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : animals.py
# @Software: PyCharm

class Animal(object):
    def run(self):
        print('animal is run ...')


class Dog(Animal):
    def run(self):
        print('dog is run')


class Cat(Animal):
    def run(self):
        print('cat is run')


a = Animal()
b = Dog()
c = Cat()
print(isinstance(a, Animal))
print(isinstance(b, Dog))
print(a, Cat)
