# -*- coding: utf-8 -*-
'''
@Time    : 2022/12/13 9:41
@Author  : robin
@File    : Dict.py
@Software: PyCharm
'''


def print_hi(name):
    print(f'Hi,{name}')
    dict = {'a': 'apple', 'b': 'banana'}
    print(dict.get('a'))
    s = set([1, 2, 3])
    s2 = set(['a', 'b', 'c'])
    s3 = s | s2
    s4 = set(['a', 1, 'b', 2])
    s5 = s3 & s4
    # print(s5)
    # print(s3)
    # s.add(4)
    # s.remove(3)
    # print(s)
    dict.pop('b')
    print(dict)


class MyObject(object):

    def __int__(self):
        self.x = 9

    def power(self):
        return self.x * self.x


if __name__ == '__main__':
    # print_hi('python')
    obj = MyObject();
    # print(type(obj))
    # print(isinstance(obj, object))
    # print(dir(obj))
    if (hasattr(obj, 'x')):
        print('no')
    else:
        setattr(obj, 'x', 10)
        print(getattr(obj, 'x'))
