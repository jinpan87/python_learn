# -*- coding: utf-8 -*-
# @Time    : 2021/11/23 9:14
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : high_fun.py
# @Software: PyCharm

# def add(x, y, f):
#     return f(x) + f(y)
#
#
# print(add(5, -6, abs))

from functools import reduce


def get_int(s):
    DESINT = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}
    return DESINT[s]


def str_int(s):
    return reduce(lambda x, y: x * 10 + y, map(get_int, s))


def fact(s):
    if s == 1:
        return 1
    else:
        return s * fact(s - 1)


# print(str_int('13579'))

if __name__ == '__main__':
    # print(get_int('3'))
    # print(fact(5))
    li = list(range(100))
    print(li[::2])
    print(li[3:7])
    tu = tuple(range(50))
    print(tu[4:9])
    print(tu[3:8])
    print('abcdefg'[2:6:2])
    print(tu[-5:-2])