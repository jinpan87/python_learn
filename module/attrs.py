# -*- coding: utf-8 -*-
# @Time    : 2021/11/3 10:27
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : attrs.py
# @Software: PyCharm

class MyObject(object):
    def __init__(self):
        self.x = 9

    def power(self):
        return self.x * self.x


obj = MyObject()

print(hasattr(obj, 'x'))
print(hasattr(obj, 'y'))
setattr(obj, 'y', 19)
print(hasattr(obj, 'y'))
print(getattr(obj, 'y'))
#setattr(obj, 'z', 404)
print(getattr(obj, 'z',90))
print(dir(obj))
