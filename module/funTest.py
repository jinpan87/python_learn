#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: funTest.py
@date: 2022/1/26 9:45
@desc:
"""


def print_hi(name):
    # print(f'Hi, {name}')
    print("start ...")
    while True:
        res = yield 4
        print(res)


def hello(greeting, *args):
    if (len(args) == 0):
        print("%s" % greeting)
    else:
        print("%s,%s" % (greeting, ', '.join(args)))


def print_score(**kw):
    print('      name score')
    for name, score in kw.items():
        print('%10s,%d' % (name, score))
    print()

def print_info(name,age,*,gender,city='beijing',addr):
    print('-----------------------------')
    print('name is %s' %name)
    print('age is %d' %age)
    print('gender %s' %gender)
    print('city is %s' %city)
    print('addr is %s' %addr)
    print()



if __name__ == '__main__':
    # g = print_hi("");
    # print(next(g))
    # print("*" * 20)
    # print(next(g.send(7)))
    # hello("hello")
    #     # hello("hello", 'bob')
    #     # hello('hello', 'bob', 'lelei')
    #     # # names = ['lily', 'lucy', 'tom', 'jery']
    #     # names = ('lily', 'lucy', 'tom', 'jery')
    #     # hello('hello', *names)
    # print_score(adam=99, lisa=88, lily=22)
    # data = {'lily': 79, 'lucy': 790, 'lilei': 80}
    # print_score(**data)
    print_info('robin',50,gender='男',addr='舞之魂')