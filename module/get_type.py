# -*- coding: utf-8 -*-
# @Time    : 2021/11/3 10:17
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : get_type.py
# @Software: PyCharm
print('type(123)=', type(123))
print('type(\'123\'=', type('123'))
print('type(none)=', type(None))
print('type(abs)=', type(abs))

import types

print('type(\'abc\')=', type('abc') == str)
