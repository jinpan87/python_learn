# -*- coding: utf-8 -*-
'''
@Time    : 2023/1/30 19:21
@Author  : robin
@File    : python_learn.py
@Software: PyCharm
'''
from functools import reduce


def print_hi(name):
    print(f'Hi,{name}')


'''
高阶函数
'''


def add(x, y, f):
    return f(x) + f(y)


def normalize(name):
    return name[0].upper() + name[1:].lower()

    pass


def prod(l):
    return reduce(lambda x, y: x * y, l[:])
    pass


def char2num(c):
    idx = int(c.index('.'))

    c[:idx]
    pass


def char2int(c):
    pass


def _odd_iter():
    n = 1
    while True:
        n = n + 2
        yield n


def _not_divisible(n):
    return lambda x: x % n > 0


def primes():
    yield 2
    it = _odd_iter()
    while True:
        n = next(it)
        yield n
        it = filter(_not_divisible(n), it)


## 高阶函数
def sort_method():
    print(sorted([2, 1, 4, 7, 1, -12, -45], key=abs))
    print(sorted(['robin', 'TOW', 'origin', 'MASTER'], key=str.lower, reverse=False))


def by_name(t):
    return t[0]


def by_sort(t):
    return t[1]


def list_sort():
    l = [("robin", 90), ("tome", 89), ("jack", 66), ("lucy", 34)]
    print(sorted(l, key=by_name))
    print(sorted(l, key=by_sort, reverse=True))


if __name__ == '__main__':
    list_sort();
    # sort_method()
    # print_hi('python')
    # print(add(4, -5, abs))
    # print(list(map(normalize, ['admn', 'ABCD', 'dafFD'])))
    # print(prod([3, 4, 5, 6]))
    # for n in primes():
    #     if n < 1000:
    #         print(n)
    #     else:
    #         break;
