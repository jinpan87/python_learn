# -*- coding: utf-8 -*-
# @Time    : 2021/11/5 13:58
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : create_class_on_the_fly.py
# @Software: PyCharm

def fn(self, name='world'):
    print('hello %s' % name)


Hello = type('Hello', (object,), dict(hello=fn))

h = Hello()
print('call h.hello():')
h.hello()
print('type(Hello)=', type(Hello))
print('type(h)=', type(h))
