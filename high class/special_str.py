# -*- coding: utf-8 -*-
# @Time    : 2021/11/5 10:45
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : special_str.py
# @Software: PyCharm

class Student(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Student object (name: %s)' % self.name

    __repr__ = __str__


print(Student('robin'))


#########
class Fid(object):
    def __init__(self):
        self.a, self.b = 0, 1

    def __iter__(self):
        return self

    def __next__(self):
        self.a, self.b = self.b, self.a + self.b
        if self.a > 10000:
            raise StopIteration
        return self.a


for n in Fid():
    print(n)
##########

