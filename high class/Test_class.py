# import unittest
#
#
# class MyTestCase(unittest.TestCase):
#     def test_something(self):
#         self.assertEqual(True, False)  # add assertion here


class Student(object):
    __slots__ = ('name', 'age')


class HighStudent(Student):
    pass


if __name__ == '__main__':
    s = Student()
    s.name = 'mikel'
    s.age = 30
    try:
        s.score = 99
    except AttributeError as e:
        print('AttributeArr:', e)
    g = HighStudent()
    g.score = 90
    print(g.score)
