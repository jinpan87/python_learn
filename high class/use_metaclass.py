# -*- coding: utf-8 -*-
# @Time    : 2021/11/5 14:05
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : use_metaclass.py
# @Software: PyCharm
class ListMetaclass(type):
    def __new__(cls, name, bases, attrs):
        attrs['add'] = lambda self, value: self.append(value)
        return type.__new__(cls, name, bases, attrs)


class MyList(list, metaclass=ListMetaclass):
    pass


L = MyList()
L.append(1)
L.append(2)
L.append(3)
L.append('END')
print(L)
