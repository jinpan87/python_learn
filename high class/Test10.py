#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: Test10.py
@date: 2022/3/10 9:19
@desc:
"""


def print_hi(name):
    print(f'Hi, {name}')


def trim(s):
    length = len(s)
    if length > 0:
        for i in range(length):
            if s[i] != ' ':
                break;
        j = length - i
        while s[j] == ' ' and j >= i:
            j -= 1
        s = s[i:j + 1]
    return s;


if __name__ == '__main__':
    # print_hi('Python')
    if trim(" hello") != 'hello':
        print("测试失败")
    elif trim(" hello ") != 'hello':
        print("测试失败")
    elif trim(" hello world ") != "hello world":
        print("测试失败")
    # elif trim("     hello      "):
    #     print("测试失败")
    else:
        print("测试成功")

    l = ['java', 'c', 'c++', 'python', 'php']
    print(l[::2])
    print(l[:2])
    num = list(range(100))
    print(num[::5])
    print(num[:])
