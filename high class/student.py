# -*- coding: utf-8 -*-
# @Time    : 2021/11/5 11:15
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : student.py
# @Software: PyCharm

# class Student(object):
#     def __init__(self):
#         self.name = 'robin'
#
#     def __getattr__(self, item):
#         if item == 'score':
#             return 99
#         if item == 'age':
#             return lambda: 25
#         else:
#             raise AttributeError('\'Student\' object has no attribute \'%s \'' % item)
#
#
# s = Student()
# print(s.name)
# print(s.score)
# print(s.age())
# print(s.grade)

"""


class Student(object):
    def __init__(self, name):
        self.name = name

    def __call__(self):
        print('my name is %s ' % self.name)


s = Student('robin')
s()
"""


class Student(object):
    __slots__ = ("name", "age")


class GraduateStudent(Student):
    pass


try:
    gs = GraduateStudent()
    gs.name = "robin"
    gs.grade = "class3"
    print(gs.name)
    print(gs.grade)
except AttributeError as e:
    print("attributeError:", e)
