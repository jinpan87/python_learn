# -*- coding: utf-8 -*-
# @Author  : robin

import scrapy
from scrapy import Request


class DoubanMovieSpider(scrapy.Spider):
    name = 'douban_movie'
    allowed_domains = ['read.douban.com']
    tuple_1 = {'热门', '最新', '经典', '可播放', '豆瓣高分'}
    start_urls = [f'https://movie.douban.com/explore#!type=movie&tag={type}&sort=recommend&page_limit=20&page_start=0'
                  for type in tuple_1]
    # start_urls = ['https://movie.douban.com/explore#!type=movie&tag=热门&sort=recommend&page_limit=20&page_start=0']




    def parse(self, response):
        movie_list = response.xpath('//*[@id="content"]/div/div[1]/div/div[4]/div/a/p')
        print(movie_list.xpath('./strong').getall())

        for movie_title in movie_list:
            # title = movie_title.xpath('//*[@id="content"]/div/div[1]/div/div[4]/div/a[9]/p').get()
            grade = movie_title.xpath('./strong').get()
            print(grade)
            yield Request(
                f'https://movie.douban.com/explore#!type=movie&tag=%E6%9C%80%E6%96%B0&page_limit=20&page_start={page}'
                for page in [20, 40, 60, 80, 100])
        # yield next(self.start_urls, callable=self.parse)
        # pass
