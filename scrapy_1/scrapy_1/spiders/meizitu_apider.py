import scrapy


class MeizituSpider(scrapy.Spider):
    name = "meizitu"
    star_urls = ["https://www.516m.com/",
                 ]

    def parse(self, response):
        # print(response.xpath('//*[@id="wrapper"]/div[2]/div').getall())
        for women in response.xpath('//*[@id="wrapper"]/div[2]/div'):
            yield {
                "title": women.xpath('//div/a/div/h3').getall(),
            }
