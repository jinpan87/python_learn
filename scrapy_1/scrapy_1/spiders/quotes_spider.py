import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'http://quotes.toscrape.com/page/1/',
        # 'http://quotes.toscrape.com/page/2/',
        # 'http://quotes.toscrape.com/page/3/',
        # 'http://quotes.toscrape.com/page/1/',
        # 'http://quotes.toscrape.com/page/2/',
        # 'http://quotes.toscrape.com/page/3/',
    ]

    def parse(self, response):
        # for quote in response.css('div.quote'):
        #     yield {
        #         'text': quote.css('span.text::text').get(),
        #         'author': quote.css('small.author::text').get(),
        #         'tags': quote.css('div.tags a.tag::text').getall(),
        #     }
        for quotes in response.xpath('//html/body/div[1]/div[2]/div[1]'):
            yield {
                "author": quotes.xpath('//div/span[2]/small').get()
            }
