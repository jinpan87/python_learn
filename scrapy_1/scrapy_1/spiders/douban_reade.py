import scrapy


class DoubanReadeSpider(scrapy.Spider):
    name = 'douban_reade'
    allowed_domains = ['read.douban.com']
    start_urls = ['https://read.douban.com/category/115',
                  'https://read.douban.com/category/116',
                  'https://read.douban.com/category/117']

    def parse(self, response):
        self.logger.info(response.url)
        self.logger.info(response.xpath('//span/span').get())
        name = response.xpath('/html/body/div[1]/div[2]/div[2]/div/section[2]/div[1]/ul/li[5]/div/div[2]/div[4]/div[1]/span/span/span/text()').get()
        self.logger.info("============name=================="+name)

    pass
