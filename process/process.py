# -*- coding: utf-8 -*-
# @Time    : 2021/11/18 9:25
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : process.py.py
# @Software: PyCharm
import os
from multiprocessing import Process


def run_proc(name):
    print('运行子进程%s(%s)' % (name, os.getpid()))


if __name__ == '__main__':
    print('父进程%s' % os.getpid())
    p = Process(target=run_proc, args=('test',))
    print('子进程将开始...')
    p.start()
    p.join()
    print('子进程结束')

print("process (%s) start ..." % os.getpid())
#
# pid = os.fork()
#
# if pid == 0:
#     print("i am a child process (%s) and my parent process is (%s)" % (os.getpid(), os.getppid()))
# else:
#     print('i (%s) just oreated a child process (%s).' % (os.getpid(), pid))
