# -*- coding: utf-8 -*-
# @Time    : 2021/12/12 8:47
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : my_matplotlib.py
# @Software: PyCharm
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


# 设置字体
def find_font():
    # a = sorted([f.name for f in matplotlib.font_manager.fontManager.ttflist])
    #
    # for i in a:
    #     print(i)
    plt.rcParams['font.family'] = ['FangSong']

    FangSong = matplotlib.font_manager.FontProperties(fname="FangSong", size=18)
    return FangSong


def my_matplotlib():
    font1 = {'color': 'blue', 'size': 20}
    x = np.arange(0, 4 * np.pi, 0.1)
    y = np.sin(x)
    z = np.cos(x)
    plt.plot(x, y, '>:r', x, z, 'bo')
    plt.xlabel('x-lable', fontdict=font1)
    plt.ylabel('y-lable')
    plt.savefig('./line.png')
    plt.show()


def my_mat_color():
    ypoints = np.array([1, 3, 4, 5, 8, 9, 6, 1, 3, 4, 5, 2, 4])

    plt.plot(ypoints, marker='o', ms=40, mec='r')
    plt.show()


def print_hi(name):
    print(f'Hi, {name}')


# 网格线

def my_grid():
    x = np.array([2, 5, 8])
    y = np.array([4, 7, 9])
    plt.title('my grid')
    plt.plot(x, y, 'o--r')
    plt.xlabel('x-lable')
    plt.ylabel('y-lable')
    # plt.grid(axis='x')
    plt.grid(color='r', linestyle='--', linewidth='0.2')
    plt.savefig('./my_grid.png')
    plt.show()


if __name__ == '__main__':
    # print_hi('Python')
    # my_matplotlib()
    # my_mat_color()
    # find_font()
    my_grid()
