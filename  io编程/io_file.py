#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: io_file.py
@date: 2022/5/9 9:59
@desc:
"""


def print_hi(name):
    print(f'Hi, {name}')


##读取文件
def read_file(path):
    with open(path, 'r') as f:
        print(f.read())


def write_file(path):
    with open(path, 'a') as f:
        f.write("hello world")
        f.write("robin")


if __name__ == '__main__':
    # print_hi('Python')

    path = "D:\jurassic\plantuml_test.txt"
    write_file(path)
    read_file(path)
