import scrapy

from scrapy_demo.items import MovieItem


class MaoyanSpider(scrapy.Spider):
    name = 'maoyan'
    allowed_domains = ['maoyan.com']
    # start_urls = ['https://www.maoyan.com/films']

    def start_requests(self):
        scrapy.Request('https://www.maoyan.com/films', dont_filter=True)

    def parse(self, response):
        names = response.xpath('//*[@id="app"]/div/div[2]/div[2]/dl/dd/div[2]/a/text()').extract()
        # scores = [score.xpath('string(.)').extract_first() for score in
        #           response.xpath('//*[@id="app"]/div/div[2]/div[2]/dl/dd/div[3]')]

        scores = response.xpath('//*[@id="app"]/div/div[2]/div[2]/dl/dd/div[3]').extract()
        item = MovieItem()
        for name, score in zip(names, scores):
            # item['name'] = name
            # item['score'] = score
            # yield item
            yield {'name': name, 'score': score}
