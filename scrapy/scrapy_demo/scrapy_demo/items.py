# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapyDemoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()
    file = scrapy.Field()
    type = scrapy.Field()
    pass


class MovieItem(scrapy.Item):
    name = scrapy.Field()
    score = scrapy.Field()
