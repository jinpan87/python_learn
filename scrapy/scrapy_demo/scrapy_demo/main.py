#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: main.py
@date: 2022/1/30 21:55
@desc:
"""
from scrapy.cmdline import execute

execute('scrapy crawl maoyan'.split())

# def print_hi(name):
#     print(f'Hi, {name}')
#
#
# if __name__ == '__main__':
#     print_hi('Python')
