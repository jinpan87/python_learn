import scrapy

from myproject.items import DoubanItem



class DoubanSpider(scrapy.Spider):
    name = 'douban'
    allowed_domains = ['movie.douban.com']
    start_urls = ['https://movie.douban.com/']

    def parse(self, response):

        titles = response.xpath('//div/div[2]/div[4]/div[3]/div/div[1]/div/div/a/p/text()').extract()
        print(response.body)
        for name in titles:

            item = DoubanItem()
            item['name'] = name
            yield item

        # print('=====================' + title + '=================================')
    # movie_list = response.xpath('//*[@id="anony-movie"]/div[1]/div[3]/div')
    # for movie_name in movie_list:.
    #   yield {
    #      "title": movie_name.xpath('//ul/li[2]/div[2]/a').getall()
    # }
