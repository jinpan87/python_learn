# -*- coding: utf-8 -*-
# @Time    : 2021/12/15 19:36
# @Author  : robin
# @Email   : xxxxxxxxxxx@xxx.com
# @File    : numpy_test.py
# @Software: PyCharm
$END$
def print_hi(name): 
    print(f'Hi, {name}')
if __name__ == '__main__': 
    print_hi('Python')