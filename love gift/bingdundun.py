#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: bingdundun.py
@date: 2022/2/10 9:27
@desc:
"""


def print_hi(name):
    print(f'Hi, {name}')


if __name__ == '__main__':
    print_hi('Python')
