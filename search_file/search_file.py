#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author: robin
@license: MIT
@contact: 475405660@qq.com
@file: search_file.py
@date: 2022/4/3 6:16
@desc:
"""
import re

from docx import Document


def print_hi(name):
    with open("D:\jurassic\武汉方言词典_校对版.docx", 'rb') as f:
        document = Document(f)
        pattern = re.compile(r'((^\W(\S).\3\w)|((.)\5(.)\6))')
        # print(list(i[0] for i in pattern.findall(document)))

        print(document.styles)
        print(document.paragraphs)
        for paragraph in document.paragraphs:
            # line = paragraph.text
            # re_list = list(i[0] for i in pattern.findall(paragraph.text))
            for i in pattern.findall(paragraph.text):
                # if len(i[0]) > 0:
                print(i[0])
            # logger.info(line)

            # print(line)

    print(f'Hi, {name}')


def re_find():
    text = '''行尸走肉、金蝉脱壳、百里挑一、金玉满堂、背水一战、霸王别姬、天上人间、不吐不快、海阔天空、
    情非得已、满腹经纶、兵临城下、春暖花开、插翅难逃、黄道吉日、天下无双、偷天换日、两小无猜、卧虎藏龙、珠光宝气、
    簪缨世族、花花公子、绘声绘影、国色天香、 相亲相爱、八仙过海、金玉良缘、掌上明珠、皆大欢喜、浩浩荡荡、平平安安、秀秀气气、斯斯文文、高高兴兴'''
    # pattern = re.compile(r'(\S)\w\1')
    pattern = re.compile(r'(([^/s](\S).\3\w)|((.)\5(.)\6))')
    # pattern = re.compile(r'((.).\2.)')

    print(list(i[0] for i in pattern.findall(text)))

    # print(pattern.findall(text))
    # pattern=re.findall(r'\s', text)
    # print(pattern)
    # print(list( i in re.split(r'\、',text)))

    # pattern = r'(((. ). \3.)|((. )\5(. )\6)'
    # pattern = r'^\行\.\.\.'
    # re_list = list(i[0] for i in findall(pattern, text))
    # for item in findall(pattern, text):
    #     print(item[0])
    # print("匹配ABAC和AABB的词语：", list(i[0] for i in findall(pattern, text)))


if __name__ == '__main__':
    # print_hi('Python')
    re_find()
